#!/bin/sh

# mkoctfile -p ...
INCFLAGS="-I/usr/include/octave-3.8.1/octave/.. -I/usr/include/octave-3.8.1/octave "
LFLAGS="-L/usr/lib/x86_64-linux-gnu/octave/3.8.1 -L/usr/lib/x86_64-linux-gnu"

cd lib
gcc -c -fPIC $INCFLAGS -g -O0 -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -pthread -fopenmp -I. -I../lib -D_MEASURE_ *.c

cd ../matlab
gcc -c -fPIC $INCFLAGS -g -O0 -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -pthread -fopenmp -I. -I../lib -D_MEASURE_ number_.c    -o number_.o
g++ -shared -Wl,-Bsymbolic -o number_.mex number_.o ../lib/*.o $LFLAGS -loctinterp -loctave -Wl,-Bsymbolic-functions -Wl,-z,relro

cd ../@Number
gcc -c -fPIC $INCFLAGS -g -O0 -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -pthread -fopenmp -I. -I../lib -D_MEASURE_ subsref.c  -o subsref.o
gcc -c -fPIC $INCFLAGS -g -O0 -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -pthread -fopenmp -I. -I../lib -D_MEASURE_ subsasgn.c -o subsasgn.o
gcc -c -fPIC $INCFLAGS -g -O0 -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -pthread -fopenmp -I. -I../lib -D_MEASURE_ delete.c   -o delete.o

g++ -shared -Wl,-Bsymbolic -o  subsref.mex  subsref.o ../lib/*.o $LFLAGS -loctinterp -loctave -Wl,-Bsymbolic-functions -Wl,-z,relro
g++ -shared -Wl,-Bsymbolic -o subsasgn.mex subsasgn.o ../lib/*.o $LFLAGS -loctinterp -loctave -Wl,-Bsymbolic-functions -Wl,-z,relro
g++ -shared -Wl,-Bsymbolic -o   delete.mex   delete.o ../lib/*.o $LFLAGS -loctinterp -loctave -Wl,-Bsymbolic-functions -Wl,-z,relro
cd ..

