
#ifndef _Wrap_h
#define _Wrap_h

#include <stdio.h>
#include <include/Number.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {

  Number *number;

} Wrap;

/* creates and returns a new wrap structure */
Wrap *WrapNew(double val);

/* deletes **wrap and set *wrap=0 */
void WrapFree(Wrap **wrap);

/* prints some information about the wrap structure */
void WrapInfo(FILE *out, Wrap *wrap);

#ifdef __cplusplus
}
#endif

#endif
