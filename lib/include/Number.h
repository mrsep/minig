
#ifndef _Number_h
#define _Number_h


#include <stdio.h>
#include <stdlib.h>

#define NEW1(ptr, type, nr) if(!(ptr = (type)calloc((size_t)(nr), sizeof(*ptr))) && nr) { printf("calloc of %lu bytes failed on line %d of file %s\n", (unsigned long int)(nr * sizeof(*ptr)), __LINE__, __FILE__); printf("ptr%p\n", ptr); exit(EXIT_FAILURE); }
#define FREE(ptr)  free(ptr)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
  double value;

} Number;

/* creates and returns a new number */
Number *NumberNew(double val);

/* deletes **number and set *number=0 */
void NumberFree(Number **number);

/* sets value=0 */
void NumberReset(Number *number);

/* changes the value */
void NumberChangeValue(Number *number, double val);

/* creates and returns a copy of number */
Number *NumberCopy(Number *number); 

/* returns the value of the number */
double NumberValue(Number *number);

/* print some information about the number */
void NumberInfo(FILE *out, Number *number);     

#ifdef __cplusplus
}
#endif


#endif
