
#include <include/Number.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

Number *NumberNew(double value) {

  Number *number;

  NEW1(number, Number*, 1);
  number->value = value;

  return number;
}

void NumberFree(Number **number) {
  if (*number) {
    FREE(*number);
    *number = 0;
  }
}

void NumberReset(Number *number) {
  number->value = 0.0;
}

void NumberChangeValue(Number *number, double value) {
  number->value = value;
}

Number *NumberCopy(Number *number) {
  Number *copy;

  NEW1(copy, Number*, 1);
  copy->value = number->value;
  NumberReset(copy);

  return copy;
}

double NumberValue(Number *number) {
  return number->value;
}

void NumberInfo(FILE *out, Number *number) {
  fprintf(out, "Number %p:\n", number);
}

