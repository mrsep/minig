
#include <include/Wrap.h>
#include <include/Number.h>
#include <assert.h>
#include <stdio.h>

Wrap *WrapNew(double val) {

  Wrap *wrap;
  NEW1(wrap, Wrap*, 1);
  wrap->number = NumberNew(val);

  return wrap;
}

void WrapFree(Wrap **wrap) {
  if (*wrap) {
    NumberFree(&((*wrap)->number));
    FREE(*wrap);
    *wrap = NULL;
  }
}

void WrapInfo(FILE *out, Wrap *wrap) {
  fprintf(out, "Wrap %p:\n", wrap);
  if (wrap) {
    fprintf(out, "  Number %p:\n", wrap->number);
  }
}

