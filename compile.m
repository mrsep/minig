% Octave compile script by Julien Bect

cd lib
mex -c -I. Number.c
mex -c -I. Wrap.c

cd ../matlab
mex -I. -I../lib -D_MEASURE_ number_.c ../lib/Number.o ../lib/Wrap.o

cd ../@Number
mex -I. -I../lib -D_MEASURE_ subsref.c ../lib/Number.o ../lib/Wrap.o
mex -I. -I../lib -D_MEASURE_ subsasgn.c ../lib/Number.o ../lib/Wrap.o
mex -I. -I../lib -D_MEASURE_ delete.c ../lib/Number.o ../lib/Wrap.o
cd ..

