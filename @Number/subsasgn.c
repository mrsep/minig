
#include <mex.h>
#include <include/mxHandling.h>
#include <include/Wrap.h>
#include <string.h>

void mexFunction(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char *type, *attr;
  Wrap *wrap;
  mxArray *ptr;
  double *value;
  
  ptr = mxGetField(prhs[0], 0, "handle");
  wrap = (Wrap *)ptrFromMxArray(ptr);
  if (wrap==NULL) mexErrMsgTxt("Number: number is empty.");

  ptr = mxGetField(prhs[1], 0, "type");
  type = mxArrayToString(ptr);
  
  if (!strcmp(type,".")) {
    ptr = mxGetField(prhs[1], 0, "subs");
    attr = mxArrayToString(ptr);

    if (!strcmp(attr, "value")) {
      if (mxGetNumberOfElements(prhs[2]) == 1) {
        value = mxGetPr(prhs[2]);
        wrap->number->value = *value;
      } else {
        mexErrMsgTxt("Number.value: right hand side has wrong dimension");
      }
    } 
    mxFree(attr);
  }

  mxFree(type);
  
  plhs[0] = (mxArray*) prhs[0];

  return;
}
