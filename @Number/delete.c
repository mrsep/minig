
#include <mex.h>
#include <include/mxHandling.h>
#include <include/Wrap.h>

void mexFunction(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
				)
{
  mxArray *null;
  Wrap *wrap = (Wrap *)ptrFromMxArray(mxGetField(prhs[0], 0, "handle"));
  WrapFree(&wrap);
  null = mxCreateDoubleMatrix(1,1,mxREAL);
  *mxGetPr(null) = 0;
  mxSetField(prhs[0], 0, "handle", null);
}


