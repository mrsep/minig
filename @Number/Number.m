function n = Number(arg0)

% Number - create a Number object
%    n = Number(value) creates a Number of double type
%
%    NOTE: call delete(n); clear n; to delete the number from memory.
% 
% Attributes
%    n.value                 - current value

% 
% Methods, not yet implemented for example:
% n.change_value(value)
% n.sign()
% n.exponent()
% n.mantissa()
% n.abs()


if nargin==0
  error('Too few input arguments: values expected.')
elseif nargin==1
  if ~isfloat(arg0)
    error('double value expected');
  end
  n.handle = number_(arg0);
  n = class(n, 'Number');
end

