
#include <mex.h>
#include <include/mxHandling.h>
#include <include/Number.h>
#include <include/Wrap.h>
#include <stdio.h>

void mexFunction(
	int nlhs,       mxArray *plhs[],
	int nrhs, const mxArray *prhs[]
	)
{
	char *type, *attr;
	mxArray *ptr;
	double *pr, *pl;
	double value;
	Wrap *wrap;

	ptr = mxGetField(prhs[0], 0, "handle");
  wrap = (Wrap *)ptrFromMxArray(ptr);
	if (wrap==NULL) mexErrMsgTxt("Number: number is empty.");

	value = wrap->number->value;
    
	ptr = mxGetField(prhs[1], 0, "type");
	type = mxArrayToString(ptr);

	ptr = mxGetField(prhs[1], 0, "subs");
	attr = mxArrayToString(ptr);

	/* attributes */
	if (mxGetNumberOfElements(prhs[1])==1 && !strcmp(type,".")) {

		if (!strcmp(attr, "value")) {
			plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
			pr = mxGetPr(plhs[0]);
			pr[0] = value;
		}
		else if (!strcmp(attr, "info")) {
			plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
			pr = mxGetPr(plhs[0]);
			WrapInfo(stdout, wrap);
			NumberInfo(stdout, wrap->number);
		}
		else {
			mexErrMsgTxt("Number: unknown field.");
		}
	}

	/* methods */
	if (mxGetNumberOfElements(prhs[1])==2 && !strcmp(type,".")) {
		int nargin, dim0 = 0;
		mxArray *cell0 = 0;

		ptr = mxGetField(prhs[1], 1, "subs");
		nargin = mxGetNumberOfElements(ptr);

    if (nargin == 0) {
      char errMsg[128];
      sprintf(errMsg, "Number.%.100s: missing arguments", attr);
      mexErrMsgTxt(errMsg);
    }

		cell0 = mxGetCell(ptr, 0);
		dim0 = mxGetNumberOfElements(cell0);

		if (!strcmp(attr, "change_value")) {        /* change_Value */
			int m_c;
			double *value;
			if (nargin !=1)
				mexErrMsgTxt("Number.change_Q: only one argument please.");
			value = mxGetPr(cell0);
			m_c = mxGetNumberOfElements(cell0);
			if (m_c != 1)
				mexErrMsgTxt("Number.change_Value: number is just a single value.");
			NumberChangeValue(wrap->number, *value);
			plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
		}
		else {
			mexErrMsgTxt("Number: unknown method.");
		}
	}
	mxFree(attr);
	mxFree(type);

	return;
}

